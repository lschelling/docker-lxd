# Run LXD inside a docker container #

## create the image

```bash
docker build --tag docker-lxd .
```

## run the container

LXD requires the docker container is running in privileged mode. The `cgroup` file must be mapped in the docker container using the `-v` option.

``bash
docker run  --privileged  -v /sys/fs/cgroup:/sys/fs/cgroup:ro  -t docker-lxd
```

or 

```bash
./start.sh
```

## execute bash


```bash
docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
740b11708468        docker-lxd          "/entrypoint.sh"    5 hours ago         Up 5 hours                              adoring_hamilto

docker exec -it 740b11708468 /bin/bash
```


